Setup:
	download GLEW binaries: http://glew.sourceforge.net/
	download freeglut binaries with http://www.transmissionzero.co.uk/software/freeglut-devel/ or sources and build it (http://freeglut.sourceforge.net/)
	download GLM header-only library from https://sourceforge.net/projects/ogl-math/
	configure:
	 - header include paths
	 - add library files into linker .lib 
	 - library seachr paths
	 - add .dll into project directory

	see video: https://www.youtube.com/watch?v=8p76pJsUP44

